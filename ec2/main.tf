resource "aws_vpc" "my_vpc" {
  cidr_block =  var.cidr_block

  tags = {
    Name = "tf-example"
  }
}

resource "aws_subnet" "my_subnet" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = var.subnet_cidr
  availability_zone = "us-west-2b"

  tags = {
    Name = "tf-example"
  }
}