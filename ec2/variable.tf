 variable "cidr_block" {
     type = string
     default = "172.16.0.0/16"
 }

 variable "subnet_cidr" {
     type = string
     default = "172.16.10.0/24"
 }